import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { undoItem, redoItem } from '../actions';


const WrapButton = styled.div`
    display: flex;
    margin-top: 16px;

`;
const Button = styled.button`
    padding: 8px 16px;
    background: #b3a56a;
    margin-right: 8px;
    outline: none;
    border-radius: 4px;
    opacity: ${props => props.disabled ? 0.5 : 1};
`;



 function UndoRedoAction({ canUndo, canRedo, dispatch }) {
     const handleUndoAction = () => {
         dispatch(undoItem());
     }
     const handleRedoAction = () => {
        dispatch(redoItem());
    }

    

    return (
        <WrapButton>
            <Button  disabled = {canUndo} onClick = {handleUndoAction}>
                Undo
            </Button>
            <Button  disabled = {canRedo} onClick = {handleRedoAction}>
                Redo
            </Button>
     </WrapButton>

    )
}

const mapStateToProps = ({ items }) => {
   return {
       canUndo: !(items.items.length > 0),
       canRedo: !(items.tmpItems.length > 0)
   }

}

export default connect(mapStateToProps)(UndoRedoAction);