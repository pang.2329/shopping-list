import React, { useState, useEffect } from 'react';

import styled from 'styled-components';

const Item = styled.div`
    padding: 8px 16px;
    margin: 8px;

    &:hover {
        opacity: 0.5;
        cursor: pointer;
    }

`; 

const Label = styled.div`
    text-decoration: ${props => !props.isActive ? 'line-through;' : 'none'};

`; 


function ShoppingItem({ id, label, isActive, onClick }) {

   useEffect(() => {

   }, [id])
    return (
        <Item onClick={onClick}>
            <Label isActive={isActive}>{label}</Label>

        </Item>

        
    );

}

export default ShoppingItem;


