import React, { useState } from 'react';

import styled from 'styled-components';

const WrapList = styled.div`



`; 
const Input = styled.input`
    width: 300px;
    height: 30px;
    padding: 8px 16px;
    margin-right: 8px;
    outline: none;
    border-radius: 4px;
    font-size: 1rem;

`;

const AddButton = styled.button`
    padding: 8px 16px;
    outline: none;
    background: ${props => props.disabled ? '#666' : 'tomato' };
    border: none;
    border-radius: 4px;
    pointer-events: ${props => props.disabled ? 'none' : 'default'};
    opacity: ${props => props.disabled ? 0.5 : 1 };


    &:hover {
        opacity: 0.5;
        cursor: pointer;
    }


`;

function AddList(props) {
    const [item, setItem] = useState('');
    const { onSubmit } = props;

    const handleSetItem = (e) => {
        setItem(e.target.value);
    }
    const handleSubmit = () => {
        onSubmit(item);
        setItem('');
    }
    return (
        <WrapList>
            <Input
                value={item}
                onChange={handleSetItem}
            
            />
            <AddButton disabled = { item === ''} onClick={handleSubmit}> Add Item</AddButton>
        </WrapList>

        
    );

}

export default AddList;


