import map from 'lodash/map';

import {
    ADD_ITEM,
    SELECT_ITEM,
    UNDO_ITEM,
    REDO_ITEM,

   
} from '../actionTypes';

const initialState = {
    items: [],
    tmpItems: []
}

const selectItem = (items, id) => {
    return map(items, item => {
        if(item.id === id) {
            return {
                ...item,
                isActive: !item.isActive
            }
        }
        return item
    })
}

const undoItem = (state) => {
    const { items, tmpItems } = state;
    const oldItems = items[items.length - 1];
    return {
        ...state,
        items: items.slice(0, items.length - 1),
        tmpItems: [
            ...tmpItems,
            oldItems
        ]
    }

}

const redoItem = (state) => {
    const { items, tmpItems } = state;

    const oldItems = tmpItems[tmpItems.length - 1];
    return {
        ...state,
        items: [
            ...items,
            oldItems
        ],
        tmpItems: tmpItems.slice(0, items.length - 1),
    }

}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_ITEM:
            return {
                ...state,
                items: [
                    ...state.items,
                    action.data
                ]
            }
        case SELECT_ITEM:
            return {
                ...state,
                items: selectItem(state.items, action.data)}

        case UNDO_ITEM:
            return undoItem(state)
        case REDO_ITEM:
            return redoItem(state)
        default:
            return state;
    }

}