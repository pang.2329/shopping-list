import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import logger from 'redux-logger';
import thunk from 'redux-thunk';

import reducers from './reducers';


import Pages from './pages';

const middleware = applyMiddleware(logger, thunk);
const store = createStore(reducers, middleware);

function App() {
  return (
    <Provider store = {store}>
      <Pages />
    </Provider>
  );
}

export default App;
