import {
    ADD_ITEM,
    SELECT_ITEM,
    UNDO_ITEM,
    REDO_ITEM,
} from '../actionTypes';

export const addItem = (item) => {
    return {
        type: ADD_ITEM,
        data: item
    };
}

export const selectItem = (itemId) => {
    return {
        type: SELECT_ITEM,
        data: itemId
    };

}

export const undoItem = () => {
    return {
        type: UNDO_ITEM,
    };

}

export const redoItem = () => {
    return {
        type: REDO_ITEM,
    };

}

