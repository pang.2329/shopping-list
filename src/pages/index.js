import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import map from 'lodash/map';
import filter from 'lodash/filter';
import find from 'lodash/find';
import get from 'lodash/get';




import { connect } from 'react-redux';

import { addItem, selectItem } from '../actions';

import { AddList, ShoppingItem, UndoRedoAction } from '../components';


const Container = styled.div`
    width: 100vw;
    min-height: 100vh;
    background: #f7f7f7;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    flex-direction: column;
    padding-top: 72px;

`;

const Header = styled.h1`
    font-size: 2em;
    font-weight: bold;
    color: #666;
`;

const FilterAction = styled.div`
    display: flex;
    margin-bottom: 16px;


`;

const FilterHeader = styled.div`

`;

const FilterButton = styled.button`
    padding: 8px 16px;
    border-radius: 8px;
    background: ${props => props.active ? '#234443' : 'transparent'};
    color: ${props => props.active ? '#fff' : '#234443'};
    margin-right: 8px;
    outline: none;
    &:hover {
        cursor: pointer;
        opacity: 0.5;
    }
`;

const Label = styled.div`
display: inline-block;
    font-weight: bold;
    font-size: 16px;
    margin-right: 8px;
    margin-bottom: 16px;
`;



 function Pages({ _items, dispatch }) {
     const [items, setItems] = useState([]);
     const [filterType, setFilterType] = useState('all');


   

    useEffect(() => {
        if (filterType === 'active') {
            setItems( filter(_items, item => item.isActive));
        } else if (filterType === 'completed') {
            setItems( filter(_items, item => !item.isActive));
        } else {
            setItems(_items);
        }

    }, [_items, filterType])




    const handleAddShoppingList = (val) => {
        console.log('handleAddShoppingList', val);
        dispatch(addItem({
            id: items.length + 1,
            name: val,
            isActive: true
        }))
    }
    const handleselectItem = (itemId) => {
        dispatch(selectItem(itemId))
    }

    const handleFilterItem = (type) => {
        console.log('handleFilterItem', type)
        setFilterType(type);
    }

    const filterList = [
        {
            id: 'all',
            label: 'All'
        },
        {
            id: 'active',
            label: 'Active'
        },
        {
            id: 'completed',
            label: 'Completed'
        }
    ]
    return (
        <Container>
            <Header>Shopping List</Header>
            <FilterAction>
                <label>Show: </label>
                {
                    map(filterList, i => (
                        <FilterButton
                            key={i.id}
                            active = { i.id === filterType}
                            onClick = {
                                () => handleFilterItem(i.id)}
                            >
                                {i.label}
                        </FilterButton>
                    ))
                }
            </FilterAction>
            <FilterHeader>
                <Label>Type: </Label>
                <Label>{get(find(filterList, ty => ty.id === filterType), 'label')}</Label>
                <Label>{ items.length > 0 && items.length}</Label>

            </FilterHeader>

            <AddList onSubmit={ handleAddShoppingList }/>
            <div>
            {
                map(items, (item, index) => {
                    return (
                        <ShoppingItem
                            key = {item.id}
                            id = {item.id}
                            label = {`${index + 1}: ${item.name}`}
                            isActive = {item.isActive}
                            onClick = { () => handleselectItem(item.id) }
                        />
                    )

                })
            }
            </div>
            <UndoRedoAction />
        </Container>
    )
}

const mapStateToProps = (state) => {
    const { items } = state;

    return {
        _items: items.items
    }

}

export default connect(mapStateToProps)(Pages);